package org.example;

public class Id {
    String nama;
     int salary;
     int np;

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setSalary(int salary){
        this.salary = salary;
    }

    public void setNp(int np) {
        this.np = np;
    }

    public String getNama() {
        return nama;
    }

    public int getSalary(){
        return salary;
    }

    public int getNp() {
        return np;
    }
}